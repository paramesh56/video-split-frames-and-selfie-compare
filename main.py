import cv2
import numpy as np


selfie = cv2.imread('selfie.jpg')

cap = cv2.VideoCapture('video.mp4')


fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output.avi', fourcc, 20.0, (640, 360))

while (cap.isOpened()):
    ret, frame = cap.read()
    if ret == True:

       
        result = cv2.matchTemplate(frame, selfie, cv2.TM_CCOEFF_NORMED)
        threshold = 0.8
        loc = np.where(result >= threshold)

        for pt in zip(*loc[::-1]):
            cv2.rectangle(frame, pt, (pt[0] + selfie.shape[1], pt[1] + selfie.shape[0]), (0, 0, 255),2)

        
        out.write(frame)

        cv2.imshow('frame', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

cap.release()
out.release()
cv2.destroyAllWindows()
